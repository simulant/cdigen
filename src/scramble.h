#pragma once

#include <fstream>
#include <optional>
#include <vector>

std::vector<char> scramble(std::vector<char>& unscrambled_bin);